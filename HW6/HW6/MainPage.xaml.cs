﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Plugin.Connectivity;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;

namespace HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ListDef Listdef;
        public MainPage()
        {
            InitializeComponent();
        }

        private bool CheckConnectivity() // Check if the user have Internet
        {
            var IsConnected = CrossConnectivity.Current.IsConnected;
            return IsConnected;
        }

        public Label createText(string name) // Create Text label
        {
            Label label = new Label();
            label.SetBinding(Label.TextProperty, name);
            label.LineBreakMode = LineBreakMode.WordWrap;
            // Change label design according to the name
            if (name == "type")
            {
                label.FontSize = 18;
                label.FontAttributes = FontAttributes.Italic;
                label.TextColor = Color.Black;
            }
            else if (name == "definition")
            {
                label.FontSize = 20;
                label.TextColor = Color.Black;
            }
            else if (name == "example")
            {
                label.FontSize = 18;
                label.TextColor = Color.Gray;
            }

            return label;
        }

        public ViewCell createListCell() // Create Cells for the Listview
        {
            return new ViewCell
            {
                View = new StackLayout
                {
                    Padding = 10,
                    Children = {
                        // Create Text label for each category
                        // Thanks to the Binding in createText function, the string (e.g. 'type') must be the same than strings in Words Class to put the value automatically in the text label
                        createText("type"),
                        createText("definition"),
                        createText("example")
                    }
                }
            };
        }

        async void Entry_Completed(object sender, EventArgs e) // When user pressing Enter to search word
        {
            if (((Entry)sender).Text != "") { // Check if the entry is empty or not
                HttpClient client = new HttpClient();
                // We need this paramaters to send the request
                string url_uri = "https://owlbot.info/api/v4/dictionary/";
                string token = "c6b8c2d01b87c2c033b554c9aa4e464deffc1161";
                // Authorization to the request
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", token);

                var request = new HttpRequestMessage();
                // The request to owlbot api
                request.RequestUri = new Uri(string.Format(url_uri + ((Entry)sender).Text, string.Empty));

                if (CheckConnectivity()) // Check if the user have Internet
                {
                    HttpResponseMessage response = await client.GetAsync(request.RequestUri);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        HttpContent content = response.Content;
                        // Get the JSON
                        var json = await content.ReadAsStringAsync();
                        //  Deserialize the JSON to ListDef
                        Listdef = JsonConvert.DeserializeObject<ListDef>(json);

                        // Create the ListView
                        ListView listView = new ListView
                        {
                            ItemsSource = Listdef.definitions,
                            ItemTemplate = new DataTemplate(createListCell),
                            HasUnevenRows = true,
                        };
                        if (mainPage.Children.Count >= 3) // Check if there is a list
                        {
                            // Delete the old listview and the old typed word
                            mainPage.Children.RemoveAt(mainPage.Children.Count - 1);
                            mainPage.Children.RemoveAt(mainPage.Children.Count - 1);
                        }
                        // Add the typed word
                        mainPage.Children.Add(new Label { Text = ((Entry)sender).Text, TextColor = Color.FromHex("#77d065"), FontSize = 30, FontAttributes = FontAttributes.Bold, Padding = 10});
                        // Add the created list
                        mainPage.Children.Add(listView);

                        // Empty entry
                        mainEntry.Text = "";
                    }
                    else
                    {
                        await DisplayAlert("Error", response.ReasonPhrase, "OK");
                    }
                }
                else
                    await DisplayAlert("Error", "You need internet to run this application", "OK");
            }
        }
    }
}
