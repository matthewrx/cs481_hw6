﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace HW6
{
    class Words
    {
        public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
    }
}